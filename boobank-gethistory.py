#!/bin/python
import sys
import os
import subprocess
from pathlib import Path
from datetime import datetime, timedelta

ACCOUNTS = {
    "Perso": {
        "account": "60245894702@cragr",
        "directory": "Documents/Finances/Crédit Agricole/Compte perso"
    },
    "Livret A": {
        "account": "60245894927@cragr",
        "directory": "Documents/Finances/Crédit Agricole/Livret A"
    },
    "Commun": {
        "account": "60245894944@cragr",
        "directory": "Documents_Communs/Finances/Crédit Agricole/Compte commun"
    },
    "Compte BPVF": {
        "account": "CPT09019855528@banquepopulaire",
        "directory": "Documents/Finances/BPVF"
    },
    "Boursorama": {
        "account": "00040482978@boursorama",
        "directory": "Documents/Finances/Boursorama"
    }
}

HOMEDIR = Path.home()
VERBOSITY_LEVELS = [
    "ERROR",
    "INFO",
    "DEBUG",
    "TRACE"
]

VERBOSITY = 2

def _print(msg, level=1):
    if level <= VERBOSITY:
        print('{}: {}'.format(VERBOSITY_LEVELS[level], msg))

_exitcode = 0
_today = datetime.now().date()
for name, _def in ACCOUNTS.items():
    _print('Getting "{}" account history'.format(name))
    _dir = os.path.join(HOMEDIR, _def["directory"])
    _state = os.path.join(_dir, ".boobank.state")
    if os.path.exists(_state):
        _last = datetime.fromtimestamp(os.stat(_state).st_mtime).date()
    else:
        _last = _today - timedelta(days=90)
    _print('Last execution: {}'.format(_last))
    if _last > _today - timedelta(days=1):
        _print('Skipping account')
        continue
    _command = [
        "boobank",
        "-n", "0",
        "history",
        "--condition=date>" + (_last - timedelta(days=1)).strftime('%Y-%m-%d'),
        _def["account"],
        "-f", "ofx"
    ]
    _print('Run command: {}'.format(_command), 2)
    _outfile = os.path.join(_dir, 'history_' + _today.strftime('%Y-%m-%d') + '.ofx')
    _rp = subprocess.run(_command, capture_output=True)
    _print('Command result: {}'.format(_rp), 3)
    if _rp.returncode != 0:
        _print('Error with command {}: {}'.format(_command, _rp.stderr.decode('UTF-8')), 0)
        _exitcode = 1
    else:
        with open(_outfile, 'w') as _out:
            _out.write(_rp.stdout.decode('UTF-8'))
        Path(_state).touch()

sys.exit(_exitcode)
