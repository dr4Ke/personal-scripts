#!/bin/bash
PROG=$(basename "$0")
DIRNAME=$(readlink -f "$(dirname "$0")")

usage () {
    echo "usage: ${PROG} FICHIER_SPECS"
    exit 2
}

FILE="$1"
[ ! -r "${FILE}" ] && usage

NOTFOUND="${DIRNAME}/${PROG}.not_found"
[ -f "${NOTFOUND}" ] && rm -f "${NOTFOUND}"
DIRNOTFOUND="${DIRNAME}/${PROG}.dir_not_found"
[ -f "${DIRNOTFOUND}" ] && rm -f "${DIRNOTFOUND}"

cd /var
while read file; do
    notfound_length=$((${#notfound_file} + 1))
    if [ "${file:0:notfound_length}" = "${notfound_file}/" ]; then
        echo "Ignoring file inside '${notfound_file}' not found directory" >&2
        echo "${file}" >> "${DIRNOTFOUND}"
        continue
    fi
    file_line=$(grep "${file}" /root/lnxjy156_var_owner_group_perms.list)
    if [ -z "${file_line}" ]; then
        # Check other files in directory
        echo "File not found in the list: ${file}" >&2
        echo "${file}" >> "${NOTFOUND}"
        notfound_file="${file}"
        continue
    fi
    IFS=';' read _f owner group perms < <(echo "${file_line}")
    current_owner=$(stat -c %U "${file}")
    current_group=$(stat -c %G "${file}")
    current_perms=$(stat -c %a "${file}")
    if [ "${owner}" != "${current_owner}" -o "${group}" != "${current_group}" ]; then
        echo "Change owner and/or group for '${file}' from '${current_owner}:${current_group}' to '${owner}:${group}'"
        chown -h ${owner}:${group} "${file}"
    fi
    if [ -L "${file}" ]; then
        echo "Ignoring symbolic link '${file}'" >&2
        continue
    fi
    if [ "${perms}" != "${current_perms}" ]; then
        echo "Change permissions for '${file}' from '${current_perms}' to '${perms}'"
        chmod ${perms} "${file}"
    fi
done < <(find .)
